<%@page import="java.util.*"
		import="vn.edu.vnuk.record.dao.MotorbikeDao"
		import="vn.edu.vnuk.record.model.Motorbike"
	
%>
<html>
	<body>
		<table>
			<%
				MotorbikeDao dao = new MotorbikeDao();
				List<Motorbike> motorbikes = dao.read();	
				for(Motorbike motorbike : motorbikes){				
			%>
			<tr>
				<td><%=motorbike.getBrand() %></td>
				<td><%=motorbike.getModel() %></td>
				<td><%=motorbike.getEngineSize() %></td>
				<td><%=motorbike.getGearBox() %></td>			
			</tr>
			<% 
				}
			%>
		</table>
	</body>
</html>