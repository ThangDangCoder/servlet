
<html>
	<body>
		<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
		<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"  %>
		<jsp:useBean id="dao" class="vn.edu.vnuk.record.dao.ContactDao"></jsp:useBean>
		
		<c:import url="header.jsp"></c:import>
		<table>
			<c:forEach var="contact" items="${dao.read()}">
			
				<tr>
					<td>${contact.name}</td>
					<td>
						<c:choose>
							<c:when test = "${not empty contact.email}">
								<a href="mailto:${contact.email}">${contact.email}</a>
							</c:when>
							
							<c:otherwise>
								<i>No email address</i>
							</c:otherwise>	
						</c:choose>
					</td>
					<td>${contact.address}</td>
					<td>
						<fmt:formatDate value="${contact.dateOfBirth.time}" pattern="dd/MM/yyyy"/></td>			
				</tr>
			</c:forEach>
		</table>
		<c:import url="footer.jsp"></c:import>
	</body>
</html>