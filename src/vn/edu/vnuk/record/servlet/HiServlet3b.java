package vn.edu.vnuk.record.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@SuppressWarnings("serial")
@WebServlet(name = "MyServlet3b", urlPatterns = {"/xin-chao", "/chao-buoi-sang"})
public class HiServlet3b extends HttpServlet {
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out  = response.getWriter();
		out.println("<html>");
		out.println("<head>");
		out.println("<title>Project vnuk-record</title>");
		out.println("</head>");
		out.println("<body>");
		out.println("<h1>Our first servlet3b :)</h1>");
		out.println("</body>");
		out.println("</html>");

	}
}
