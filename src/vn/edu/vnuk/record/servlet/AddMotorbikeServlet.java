package vn.edu.vnuk.record.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import vn.edu.vnuk.record.dao.MotorbikeDao;
import vn.edu.vnuk.record.model.Motorbike;


@WebServlet("/addMotorbike")
@SuppressWarnings("serial")
public class AddMotorbikeServlet extends HttpServlet{
	
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		log("Starting servlet");
	}
	
	@Override
	public void destroy() {
		super.destroy();
		log("Ending servlet");
	}
	
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		PrintWriter out = response.getWriter();
		String brand = request.getParameter("brand");
		String model = request.getParameter("model");
		int engineSize = Integer.parseInt(request.getParameter("engine_size"));
		String gearBox = request.getParameter("gear_box");
		
		Motorbike motorbike = new Motorbike();
		motorbike.setBrand(brand);
		motorbike.setModel(model);
		motorbike.setEngineSize(engineSize);
		motorbike.setGearBox(gearBox);
		
		MotorbikeDao dao = new MotorbikeDao();
		try {
			dao.create(motorbike);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		out.println("<html>");
		out.println("<head>");
		out.println("<title>Project vnuk-record</title>");
		out.println("</head>");
		out.println("<body>");
		out.println("<h1>Add motorbike is ok :)</h1>");
		out.println("</body>");
		out.println("</html>");
	}
}
