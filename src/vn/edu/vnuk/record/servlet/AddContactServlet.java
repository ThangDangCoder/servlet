package vn.edu.vnuk.record.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import vn.edu.vnuk.record.dao.ContactDao;
import vn.edu.vnuk.record.model.Contact;

@WebServlet("/addContact")
@SuppressWarnings("serial")
public class AddContactServlet extends HttpServlet{
	
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		log("Starting servlet");
	}
	
	@Override
	public void destroy() {
		super.destroy();
		log("Ending servlet");
	}
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		PrintWriter out = response.getWriter();
		String name = request.getParameter("name");
		String email = request.getParameter("email");
		String address = request.getParameter("address");
		String dateAsString = request.getParameter("date_of_birth");
		
		Calendar dateOfBirth = null;
		try {
			Date date = new SimpleDateFormat("dd/MM/yyyy").parse(dateAsString);
			dateOfBirth = Calendar.getInstance();
			dateOfBirth.setTime(date);
		} catch (ParseException e) {
			out.println("Error while converting day of birh...!");
			return;
		}
		
		Contact contact = new Contact();
		contact.setName(name);
		contact.setEmail(email);
		contact.setAddress(address);
		contact.setDateOfBirth(dateOfBirth);
		
		ContactDao contactDao = new ContactDao();
		try {
			contactDao.create(contact);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		out.println("<html>");
		out.println("<head>");
		out.println("<title>Project vnuk-record</title>");
		out.println("</head>");
		out.println("<body>");
		out.println("<h1>Add contact is ok :)</h1>");
		out.println("</body>");
		out.println("</html>");
	}
}
