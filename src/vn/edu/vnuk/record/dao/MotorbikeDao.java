package vn.edu.vnuk.record.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import vn.edu.vnuk.record.jdbc.ConnectionFactory;
import vn.edu.vnuk.record.model.Motorbike;

public class MotorbikeDao {
	private Connection connection;

	public MotorbikeDao() {
		this.connection = new ConnectionFactory().getConnection();
	}
	
	public void create(Motorbike motorbike) throws SQLException {
		
		String sqlQuery = "insert into motorbikes (brand, model, engine_size, gear_box) "
				+ "values (?, ?, ?, ?)";
		PreparedStatement statement;
		
		try {
			statement = connection.prepareStatement(sqlQuery);
			statement.setString(1, motorbike.getBrand());
			statement.setString(2, motorbike.getModel());
			statement.setInt(3, motorbike.getEngineSize());
			statement.setString(4, motorbike.getGearBox());
			
			statement.execute();
			statement.close();
			System.out.println("New Record in DB!");
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			connection.close();
			System.out.println("Done!");
		}
	}
	
	private Motorbike buildMotorbike(ResultSet results) throws SQLException {
		
		Motorbike motorbike = new Motorbike();
		motorbike.setId(results.getLong("id"));
		motorbike.setBrand(results.getString("brand"));
		motorbike.setModel(results.getString("model"));
		motorbike.setEngineSize(results.getInt("engine_size"));
		motorbike.setGearBox(results.getString("gear_box"));
		
		return motorbike;
	}
	@SuppressWarnings("finally")
	public List<Motorbike> read() throws SQLException {
		
		String sqlQuery = "select * from motorbikes";
		PreparedStatement statement;
		List<Motorbike> motorbikes = new ArrayList<Motorbike>();
		
		try {
			statement = connection.prepareStatement(sqlQuery);
			ResultSet results = statement.executeQuery();
			while(results.next()) {
				motorbikes.add(buildMotorbike(results));
			}
			
			statement.execute();
			results.close();
			statement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			connection.close();
			return motorbikes;
		}
	}
	
	@SuppressWarnings("finally")
	public Motorbike read(long id) throws SQLException {
		Motorbike motorbike = new Motorbike();
		
		String sqlQuery = "select * from motorbikes where id = '" + id + "'";
		PreparedStatement statement;
		
		try {
			statement = connection.prepareStatement(sqlQuery);
			ResultSet results = statement.executeQuery();
			while(results.next()) {
				motorbike = buildMotorbike(results);
			}
			
			statement.execute();
			results.close();
			statement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			connection.close();
			return motorbike;
		}
	}
	
	public void update(Motorbike motorbike) throws SQLException {
		String sqlQuery = "UPDATE motorbikes SET brand = ?, model = ?, engine_size = ?, gear_box = ? WHERE id = ?" ;
		PreparedStatement statement;
		
		try {
			statement = connection.prepareStatement(sqlQuery);
			statement.setString(1, motorbike.getBrand());
			statement.setString(2, motorbike.getModel());
			statement.setInt(3, motorbike.getEngineSize());
			statement.setString(4, motorbike.getGearBox());
			statement.setLong(5, motorbike.getId());
			statement.execute();
			statement.close();
			System.out.println("Successfully updated!");
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			connection.close();
		}
	}
	
	public void destroy(long id) throws SQLException {
		String sqlQuery = "DELETE FROM motorbikes WHERE id = ?" ;
		PreparedStatement statement;
		
		try {
			statement = connection.prepareStatement(sqlQuery);
			statement.setLong(1, id);
			statement.execute();
			statement.close();
			System.out.println("Successfully destroyed!");
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			connection.close();
		}
	}
	
	
}
